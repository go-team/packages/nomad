## https://www.nomadproject.io/docs/agent/configuration/client.html

client {
    enabled = true

    ## Optional; Default: "[data_dir]/alloc"
    #alloc_dir = "/var/lib/nomad/alloc"
}
