#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DH_GOLANG_GO_GENERATE := 1

export GOPROXY=off
export GO111MODULE=off

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DH_GOLANG_EXCLUDES := demo e2e internal/testing/apitests
export DH_GOLANG_INSTALL_EXTRA = \
    command/agent/test-resources \
    drivers/docker/test-resources \
    drivers/java/test-resources \
    helper/raftutil/generate_msgtypes.sh \
    jobspec/test-fixtures \
    nomad/structs/generate.sh \


ifneq (,$(filter $(DEB_BUILD_ARCH), mipsel mips64el))
    MINUS:=-
endif

GOTAGS:= release ui nonvidia

%:
	dh $@ --buildsystem=golang --with=golang,bash-completion --builddirectory=_build

override_dh_clean:
	dh_clean
	## Remove Files-Excluded (when built from checkout or non-DFSG tarball):
	$(RM) -rv `perl -0nE 'say $$1 if m{^Files\-Excluded\:\s*(.*?)(?:\n\n|Files:|Comment:)}sm;' debian/copyright`
	-find vendor -mindepth 1 -type d -empty -delete -printf 'removed %p\n'
	-find . -type f -name '*.pb.go' -not -name 'csi.pb.go' -delete -printf 'removed %p\n'            ## delete generated .pb.go files

#	Some vendored components (hashicorp/go-msgpack, github.com/ugorji/go) have un-generateable files.
#	-find . -type f -name '*.generated.go' -delete -printf 'removed %p\n'     ## delete .generated.go files

override_dh_auto_configure:
	dh_auto_configure
	
####    Build and install "codecgen" for temporary use:
#	export DH_GOPKG="github.com/hashicorp/nomad/vendor/github.com/ugorji/go/codec" \
#	&& dh_auto_build -v \
#	&& dh_auto_install -v -- --no-source

override_dh_auto_build:
	export GOPATH=$(CURDIR)/_build \
	&& PATH="$(CURDIR)/_build/bin:$$PATH" \
           $(MAKE) -C _build/src/github.com/hashicorp/nomad --makefile=$(CURDIR)/GNUmakefile proto
	
####    Setting PATH so custom "codegen" could be found.
	PATH="$(CURDIR)/_build/bin:$$PATH" \
	dh_auto_build -v -- -a -tags "$(GOTAGS)"

override_dh_auto_test:
	$(MINUS)PATH="$(CURDIR)/_build/bin:$$PATH" \
        DH_GOLANG_EXCLUDES="${DH_GOLANG_EXCLUDES} client api command devices/gpu drivers/docker drivers/shared nomad/deploymentwatcher" \
        skipOnlineTestsEnvVar=1 \
        dh_auto_test -v --max-parallel=3 -- -short -failfast -timeout 7m -tags "$(GOTAGS)"

override_dh_auto_install:
	dh_auto_install --destdir=debian/tmp

override_dh_install:
	dh_install -XLICENSE

override_dh_installinit:
	dh_installinit --no-start

override_dh_installsystemd:
	dh_installsystemd --no-enable --no-start
